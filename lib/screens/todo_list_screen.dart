import 'package:flutter/material.dart';
import 'package:hive_db/hive_service/hive_service.dart';
import 'package:hive_db/hive_service/models/todo.dart';
import 'package:hive_db/screens/add_todo_screen.dart';
import 'package:hive_db/hive_service/boxes.dart';

class ToDoListScreen extends StatefulWidget {
  final String title;

  const ToDoListScreen({Key? key, required this.title}) : super(key: key);

  @override
  State<ToDoListScreen> createState() => _ToDoListScreenState();
}

class _ToDoListScreenState extends State<ToDoListScreen> {
  List<ToDo?> itemList = [];

  init() async {
    final res = await HiveService.getBoxes<ToDo>(HiveBoxes.todo);
    setState(() {
      itemList = res;
    });
  }

  @override
  void initState() {
    init();
    super.initState();
  }

  @override
  dispose() {
    HiveService.closeBoxes(HiveBoxes.todo);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: ListView.builder(
          itemCount: itemList.length,
          itemBuilder: (context, index) {
            return Dismissible(
                background: Container(color: Colors.red),
                onDismissed: (direction) async {
                  await HiveService.deleteTodo<ToDo>(index, HiveBoxes.todo);
                },
                key: UniqueKey(),
                child: ListTile(
                  title: Text(itemList[index]?.title ?? ""),
                  subtitle: Text(itemList[index]?.description ?? ""),
                  leading: GestureDetector(
                    onTap: () async {
                      await HiveService.updateTodoState<ToDo>(index, HiveBoxes.todo,ToDo(title: "title", description: "description"));
                    },
                    child: const Icon(Icons.edit,size: 30,),),
                ));
          }),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AddTodoList()));
        },
      ),
    );
  }
}
