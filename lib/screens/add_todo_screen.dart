import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive_db/hive_service/hive_service.dart';
import 'package:hive_db/hive_service/boxes.dart';
import 'package:hive_db/hive_service/models/todo.dart';

class AddTodoList extends StatefulWidget {
  const AddTodoList({Key? key}) : super(key: key);

  @override
  State<AddTodoList> createState() => _AddTodoListState();
}

class _AddTodoListState extends State<AddTodoList> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
 String title="";
 String description="";
  validated() {
    if (_formKey.currentState?.validate() ?? false) {
      onFormSubmit();
      if (kDebugMode) {
        print("form Validate");
      }
    } else {
      if (kDebugMode) {
        print("form not Validate");
      }
      return;
    }
  }
onFormSubmit() async {
  await HiveService.addBoxes<ToDo>([ToDo(title: title, description: description)], HiveBoxes.todo);
    Navigator.of(context).pop();
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("addToDoList"),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Column(
                children: [
                  TextFormField(
                      autofocus: false,
                      onChanged: (String value){
                        title=value;
                      },
                      decoration: const InputDecoration(labelText: "title"),
                      validator: (String? value) {
                        if (value == null || value.trim().isEmpty) {
                          return "required";
                        }
                        return null;
                      }),
                  TextFormField(
                      autofocus: false,
                      onChanged: (String value){
                        description=value;
                      },
                      decoration: const InputDecoration(labelText: "description"),
                      validator: (String? value) {
                        if (value == null || value.trim().isEmpty) {
                          return "required";
                        }
                        return null;
                      }),
                  ElevatedButton(onPressed: (){
                    validated();
                  }, child: const Text("AddToDo"))
                ],
              ),
            )),
      ),
    );
  }
}
