import 'package:flutter/material.dart';
import 'package:hive_db/hive_service/boxes.dart';
import 'package:hive_db/hive_service/models/todo.dart';
import 'package:hive_db/screens/todo_list_screen.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  // Directory directory = await pathProvider.getApplicationDocumentsDirectory();
  await Hive.initFlutter();
  Hive.registerAdapter(ToDoAdapter());
  await Hive.openBox<ToDo>(HiveBoxes.todo);
  runApp(const MyApp()); // Wrap your app
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: Colors.deepPurple),
        home: ToDoListScreen(title: "todoList"));
  }
}
