import 'package:hive/hive.dart';
// import 'package:injectable/injectable.dart';

// @lazySingleton
class HiveService {

 static bool isExists<T>(String boxName) {
    print("isExists");
    final openBox= Hive.box<T>(boxName);
    int length = openBox.length;
    return length != 0;
  }

 static addBoxes<T>(List<T> items, String boxName) async {
    print("adding boxes");
    final openBox= Hive.box<T>(boxName);
    for (var item in items) {
      openBox.add(item);
    }
  }

 static Future<List<T?>> getBoxes<T>(String boxName) async {
    print("getBoxes");
    List<T?> boxList = <T>[];
    if(isExists<T>(boxName)==true){
      final openBox = Hive.box<T>(boxName);
      int length = openBox.length;
      for (int i = 0; i < length; i++) {
        boxList.add(openBox.getAt(i));
      }
    }
    print("boxList:$boxList");
    return boxList;
  }

 static Future<void> deleteTodo<T>(int index, String boxName) async {
    print("deleteTodo");
    await Hive.box<T>(boxName).deleteAt(index);
  }

 static Future<void> updateTodoState<T>(int index, String boxName,T items) async {
    print("updateTodoState");
    // final todo = Hive.box<T>(boxName).getAt(index);
    await Hive.box<T>(boxName).putAt(
      index,
      items,
    );
  }
  static Future<void> closeBoxes<T>(String boxName) async {
    print("closeBoxes");
    await Hive.box<T>(boxName).close();
  }
}